# 远程配置文件说明（[chapter_config-v1.json](https://gitee.com/wind_invade/chapter_reader/blob/master/chapter_config-v1.json)）：
```
注：已有的网站无需重复配置，如果节点有误，在之前的基础上修改！！
！！！不需要重复提交同一个网站！！！如果结构变化请在之前基础上修改！！！
chapterPath、contentPath、nextUrlPath 全站通用，随意在网站上找自己想看的小说。

- checkUrl    = "文章目录"     文章目录 URL 用于自动检测该网站是否可用，可以是该网站任意小说的目录URL
- chapterPath = "文章目录节点" 文章目录 节点的full path
- contentPath = "章节内容节点" 章节内容 节点的full path
- nextUrlPath = "下一页按钮节点"
    如果网站【章节内容】存在下一页的情况，请配置【下一页】节点的full path。
    否则不用配置
```

# 远程阅读FullPath节点获取教程

**写在前面：该步骤较为繁琐，适配的道友仅仅是无私为其他道友提供帮助。在此非常感谢各位提交网站适配的道友**
**注：已有的网站无需重复配置，如果节点有误在之前的基础上修改
！！！不需要重复提交同一个网站！！！如果结构变化请在之前基础上修改！！！
  - 新增【网站节点调试】可视化界面，调试成功获取后可一键复制配置。
  - 部分网站存在动态生成dom节点的问题，需要使用“保存网页到本地”，打开保存的文件进行FullPath获取。
  - 看完以下步骤，不会的可以加入QQ群，寻求各位道友帮助。

截图说明：
1. 章节目录节点获取，保存起来备用
![输入图片说明](images/zhangjiemulu_fullpath.png)
2. 文章内容节点获取，保存起来备用
![输入图片说明](images/neirong_fullpath.png)

3. 打开插件菜单【网站节点调试】填入获取的节点
![输入图片说明](images/wangzhanjiediantiaoshi.png)
4. 解析无问题后，一键复制配置。

如果网站已经配置过，但是节点失效，则修改该网站即可

如果没有配置过，则新增此配置

提交到gitee -> chapter_config-v1.json。（审核通过后，即可阅读该网站，有时会不及时，群中私聊我）